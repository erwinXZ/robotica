<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Teams Controller
 *
 * @property \App\Model\Table\TeamsTable $Teams
 *
 * @method \App\Model\Entity\Team[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TeamsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $teams = $this->Teams->find('all');
        $this->set([
            'teams' => $teams,
            '_serialize' => ['teams']
        ]);
        // $teams = $this->paginate($this->Teams);

        // $this->set(compact('teams'));
    }

    /**
     * View method
     *
     * @param string|null $id Team id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $team = $this->Teams->get($id);
        $this->set([
            'team' => $team,
            '_serialize' => ['team']
        ]);
        // $team = $this->Teams->get($id, [
        //     'contain' => ['Members']
        // ]);

        // $this->set('team', $team);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $team = $this->Teams->newEntity();
        if ($this->request->is('post')) {
            $team = $this->Teams->patchEntity($team, $this->request->getData());
            $saved_team = $this->Teams->save($team);
            if ($saved_team) {
                $json = [
                    'error' => 0,
                    'message' => 'El equipo se registro correctamente',
                    'data' => $saved_team
                ];
                $this->response->body(json_encode($json));
                return $this->response;
            } else {
                $json = [
                    'error' => 1,
                    'message' => $team->errors(),
                ];
                $this->response->body(json_encode($json));
                return $this->response;
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Team id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $team = $this->Teams->get($id);
        if ($this->request->is(['post', 'put'])) {
            $team = $this->Teams->patchEntity($team, $this->request->getData());
            if ($this->Teams->save($team)) {
                $message = 'Saved';
            } else {
                $message = 'Error';
            }
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
        // $team = $this->Teams->get($id, [
        //     'contain' => []
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     $team = $this->Teams->patchEntity($team, $this->request->getData());
        //     if ($this->Teams->save($team)) {
        //         $this->Flash->success(__('The team has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     }
        //     $this->Flash->error(__('The team could not be saved. Please, try again.'));
        // }
        // $this->set(compact('team'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Team id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $team = $this->Team->get($id);
        $message = 'Deleted';
        if (!$this->Teams->delete($team)) {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
        // $this->request->allowMethod(['post', 'delete']);
        // $team = $this->Teams->get($id);
        // if ($this->Teams->delete($team)) {
        //     $this->Flash->success(__('The team has been deleted.'));
        // } else {
        //     $this->Flash->error(__('The team could not be deleted. Please, try again.'));
        // }

        // return $this->redirect(['action' => 'index']);
    }
}
