<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MembersTeams Controller
 *
 * @property \App\Model\Table\MembersTeamsTable $MembersTeams
 *
 * @method \App\Model\Entity\MembersTeam[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MembersTeamsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Members', 'Teams']
        ];
        $membersTeams = $this->paginate($this->MembersTeams);

        $this->set(compact('membersTeams'));
    }

    /**
     * View method
     *
     * @param string|null $id Members Team id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $membersTeam = $this->MembersTeams->get($id, [
            'contain' => ['Members', 'Teams']
        ]);

        $this->set('membersTeam', $membersTeam);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $membersTeam = $this->MembersTeams->newEntity();
        if ($this->request->is('post')) {
            $membersTeam = $this->MembersTeams->patchEntity($membersTeam, $this->request->getData());
            if ($this->MembersTeams->save($membersTeam)) {
                $this->Flash->success(__('The members team has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The members team could not be saved. Please, try again.'));
        }
        $members = $this->MembersTeams->Members->find('list', ['limit' => 200]);
        $teams = $this->MembersTeams->Teams->find('list', ['limit' => 200]);
        $this->set(compact('membersTeam', 'members', 'teams'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Members Team id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $membersTeam = $this->MembersTeams->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $membersTeam = $this->MembersTeams->patchEntity($membersTeam, $this->request->getData());
            if ($this->MembersTeams->save($membersTeam)) {
                $this->Flash->success(__('The members team has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The members team could not be saved. Please, try again.'));
        }
        $members = $this->MembersTeams->Members->find('list', ['limit' => 200]);
        $teams = $this->MembersTeams->Teams->find('list', ['limit' => 200]);
        $this->set(compact('membersTeam', 'members', 'teams'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Members Team id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $membersTeam = $this->MembersTeams->get($id);
        if ($this->MembersTeams->delete($membersTeam)) {
            $this->Flash->success(__('The members team has been deleted.'));
        } else {
            $this->Flash->error(__('The members team could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
