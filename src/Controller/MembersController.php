<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Utility\Inflector;


/**
 * Members Controller
 *
 * @property \App\Model\Table\MembersTable $Members
 *
 * @method \App\Model\Entity\Member[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MembersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Teams']
        ];
        $members = $this->paginate($this->Members);

        $this->set(compact('members'));
    }

    /**
     * View method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => ['Teams']
        ]);

        $this->set('member', $member);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->autoRender = false;
        $this->loadModel('MembersTeams');
        $member = $this->Members->newEntity();
        $member_team = $this->MembersTeams->newEntity();
        if ($this->request->is('post')) {
            $birthday = date('Y-m-d', strtotime($this->request->getData('birthday')));
            $year = (int) substr($birthday, 0, 4);
            $month = (int) substr($birthday, 5, 2);
            $day = (int) substr($birthday, 8, 2);
            $year_now = substr(date("Y-m-d"), 0, 4);

            if ($this->request->getData('type') == 'L') {
                $member = $this->Members->patchEntity($member, $this->request->getData());
                $saved_member = $this->Members->save($member);
                if ($saved_member) {
                    $json = [
                        'error' => 0,
                        'message' => 'El participante se registro correctamente',
                        'data' => $saved_member
                    ];
                    $this->response->body(json_encode($json));
                    return $this->response;
                } else {
                    $json = [
                        'error' => 1,
                        'message' => $member->errors()
                    ];
                    $this->response->body(json_encode($json));
                    return $this->response;
                }
            } else {
                if ($this->request->data['profile']['size'] < 2097152) {
                    switch ($this->request->data['profile']['type']) {
                        case 'application/msword':
                            $ext = 'doc';
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $ext = 'docx';
                            break;
                        case 'application/pdf':
                            $ext = 'pdf';
                            break;
                        default:
                            $ext = null;
                    }

                    if (!empty($ext)) {
                        $dir_documents = WWW_ROOT . 'docs';
                        if (!is_dir($dir_documents)) {
                            mkdir($dir_documents, 0755, true);
                        }
                        if ($this->request->getData('type') == 'T') {
                            if ($year_now - $year >= 22) {
                                if ($year_now - $year == 22) {
                                    if ($month <= 10) {
                                        if ($month == 10) {
                                            if ($day <= 24) {
                                                $tutor_exists = $this->Members
                                                ->find()
                                                ->where(['identity_card' => $this->request->getData('identity_card')])
                                                ->orWhere(['email' => $this->request->getData('email')])
                                                ->first();

                                                if (!empty($tutor_exists)) {
                                                    $member_team['team_id'] = $this->request->getData('team_id');
                                                    $member_team['member_id'] = $tutor_exists['id'];
                                                    $this->MembersTeams->save($member_team);
                                                    $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                    if (!is_dir($dir_document)) {
                                                        mkdir($dir_document, 0755, true);
                                                    }
                                                    if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $tutor_exists['id'] . '_' .  Inflector::slug(strtolower($tutor_exists['surname']), '_') . '_' . Inflector::slug(strtolower($tutor_exists['name']), '_') . '_tutor' . '.' . $ext)) {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El tutor se registro correctamente',
                                                            'data' => $tutor_exists['id']
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    } else {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El tutor se registro correctamente pero el perfil no se pudo subir',
                                                            'data' => $tutor_exists['id']
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    }
                                                } else {
                                                    $member = $this->Members->patchEntity($member, $this->request->getData());
                                                    $saved_member = $this->Members->save($member);
                                                    if ($saved_member) {
                                                        $member_team['team_id'] = $this->request->getData('team_id');
                                                        $member_team['member_id'] = $saved_member->id;
                                                        $this->MembersTeams->save($member_team);
                                                        $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                        if (!is_dir($dir_document)) {
                                                            mkdir($dir_document, 0755, true);
                                                        }
                                                        if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '_tutor' . '.' . $ext)) {
                                                            $json = [
                                                                'error' => 0,
                                                                'message' => 'El tutor se registro correctamente',
                                                                'data' => $saved_member
                                                            ];
                                                            $this->response->body(json_encode($json));
                                                            return $this->response;
                                                        } else {
                                                            $json = [
                                                                'error' => 0,
                                                                'message' => 'El tutor se registro correctamente pero el perfil no se pudo subir',
                                                                'data' => $saved_member
                                                            ];
                                                            $this->response->body(json_encode($json));
                                                            return $this->response;
                                                        }
                                                    } else {
                                                        $json = [
                                                            'error' => 1,
                                                            'message' => $member->errors()
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    }
                                                }
                                            } else {
                                                $json = [
                                                    'error' => 1,
                                                    'message' => ['birthday' => ['_ageNotAllowed' => 'Debe ser mayor de 23 años o cumplir 23 años antes del 24 de Octubre'] ]
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            }
                                        } else {
                                            $tutor_exists = $this->Members
                                            ->find()
                                            ->where(['identity_card' => $this->request->getData('identity_card')])
                                            ->orWhere(['email' => $this->request->getData('email')])
                                            ->first();
                                            if (!empty($tutor_exists)) {
                                                $member_team['team_id'] = $this->request->getData('team_id');
                                                $member_team['member_id'] = $tutor_exists['id'];
                                                $this->MembersTeams->save($member_team);
                                                $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                if (!is_dir($dir_document)) {
                                                    mkdir($dir_document, 0755, true);
                                                }
                                                if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $tutor_exists['id'] . '_' .  Inflector::slug(strtolower($tutor_exists['surname']), '_') . '_' . Inflector::slug(strtolower($tutor_exists['name']), '_') . '_tutor' . '.' . $ext)) {
                                                    $json = [
                                                        'error' => 0,
                                                        'message' => 'El tutor se registro correctamente',
                                                        'data' => $tutor_exists['id']
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                } else {
                                                    $json = [
                                                        'error' => 0,
                                                        'message' => 'El tutor se registro correctamente pero el perfil no se pudo subir',
                                                        'data' => $tutor_exists['id']
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                }
                                            } else {
                                                $member = $this->Members->patchEntity($member, $this->request->getData());
                                                $saved_member = $this->Members->save($member);
                                                if ($saved_member) {
                                                    $member_team['team_id'] = $this->request->getData('team_id');
                                                    $member_team['member_id'] = $saved_member->id;
                                                    $this->MembersTeams->save($member_team);
                                                    $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                    if (!is_dir($dir_document)) {
                                                        mkdir($dir_document, 0755, true);
                                                    }
                                                    if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '_tutor' . '.' . $ext)) {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El tutor se registro correctamente',
                                                            'data' => $saved_member
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    } else {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El tutor se registro correctamente pero el perfil no se pudo subir',
                                                            'data' => $saved_member
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    }
                                                } else {
                                                    $json = [
                                                        'error' => 1,
                                                        'message' => $member->errors()
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                }
                                            }
                                        }
                                    } else {
                                        $json = [
                                            'error' => 1,
                                            'message' => ['birthday' => ['_ageNotAllowed' => 'Debe ser mayor de 23 años o cumplir 23 años antes del 24 de Octubre'] ]
                                        ];
                                        $this->response->body(json_encode($json));
                                        return $this->response;
                                    }
                                } else {
                                    $tutor_exists = $this->Members
                                    ->find()
                                    ->where(['identity_card' => $this->request->getData('identity_card')])
                                    ->orWhere(['email' => $this->request->getData('email')])
                                    ->first();

                                    if (!empty($tutor_exists)) {
                                        $member_team['team_id'] = $this->request->getData('team_id');
                                        $member_team['member_id'] = $tutor_exists['id'];
                                        $this->MembersTeams->save($member_team);
                                        $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                        if (!is_dir($dir_document)) {
                                            mkdir($dir_document, 0755, true);
                                        }
                                        if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $tutor_exists['id'] . '_' . Inflector::slug(strtolower($tutor_exists['surname']), '_') . '_' . Inflector::slug(strtolower($tutor_exists['name']), '_') . '_tutor' . '.' . $ext)) {
                                            $json = [
                                                'error' => 0,
                                                'message' => 'El tutor se registro correctamente',
                                                'data' => $tutor_exists['id']
                                            ];
                                            $this->response->body(json_encode($json));
                                            return $this->response;
                                        } else {
                                            $json = [
                                                'error' => 0,
                                                'message' => 'El tutor se registro correctamente pero el perfil no se pudo subir',
                                                'data' => $tutor_exists['id']
                                            ];
                                            $this->response->body(json_encode($json));
                                            return $this->response;
                                        }
                                    } else {
                                        $member = $this->Members->patchEntity($member, $this->request->getData());
                                        $saved_member = $this->Members->save($member);
                                        if ($saved_member) {
                                            $member_team['team_id'] = $this->request->getData('team_id');
                                            $member_team['member_id'] = $saved_member->id;
                                            $this->MembersTeams->save($member_team);
                                            $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                            if (!is_dir($dir_document)) {
                                                mkdir($dir_document, 0755, true);
                                            }
                                            if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '_tutor' . '.' . $ext)) {
                                                $json = [
                                                    'error' => 0,
                                                    'message' => 'El tutor se registro correctamente',
                                                    'data' => $saved_member
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            } else {
                                                $json = [
                                                    'error' => 0,
                                                    'message' => 'El tutor se registro correctamente pero el perfil no se pudo subir',
                                                    'data' => $saved_member
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            }
                                        } else {
                                            $json = [
                                                'error' => 1,
                                                'message' => $member->errors()
                                            ];
                                            $this->response->body(json_encode($json));
                                            return $this->response;
                                        }
                                    }
                                }
                            } else {
                                $json = [
                                    'error' => 1,
                                    'message' => ['birthday' => ['_ageNotAllowed' => 'Debe ser mayor de 23 años']]
                                ];
                                $this->response->body(json_encode($json));
                                return $this->response;
                            }
                        } elseif ($this->request->getData('type') == 'P') {
                            if ($year_now - $year >= 13 && $year_now - $year <= 19) {
                                if ($year_now - $year == 13) {
                                    if ($month <= 10) {
                                        if ($month == 10 ) {
                                            if ($day <= 24) {
                                                $member = $this->Members->patchEntity($member, $this->request->getData());
                                                $saved_member = $this->Members->save($member);
                                                if ($saved_member) {
                                                    $member_team['team_id'] = $this->request->getData('team_id');
                                                    $member_team['member_id'] = $saved_member->id;
                                                    $this->MembersTeams->save($member_team);
                                                    $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                    if (!is_dir($dir_document)) {
                                                        mkdir($dir_document, 0755, true);
                                                    }
                                                    if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext)) {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El participante se registro correctamente',
                                                            'data' => $saved_member
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    } else {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El participante se registro correctamente pero el perfil no se pudo subir',
                                                            'data' => $saved_member
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    }
                                                } else {
                                                    $json = [
                                                        'error' => 1,
                                                        'message' => $member->errors()
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                }
                                            } else {
                                                $json = [
                                                    'error' => 1,
                                                    'message' => ['birthday' => ['_ageNotAllowed' => 'Debes tener mas de 14 años cumplidos o cumplir 14 años antes del 24 de Octubre']]
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            }
                                        } else {
                                            $member = $this->Members->patchEntity($member, $this->request->getData());
                                            $saved_member = $this->Members->save($member);
                                            if ($saved_member) {
                                                $member_team['team_id'] = $this->request->getData('team_id');
                                                $member_team['member_id'] = $saved_member->id;
                                                $this->MembersTeams->save($member_team);
                                                $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                if (!is_dir($dir_document)) {
                                                    mkdir($dir_document, 0755, true);
                                                }
                                                if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext)) {
                                                    $json = [
                                                        'error' => 0,
                                                        'message' => 'El participante se registro correctamente',
                                                        'data' => $saved_member
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                } else {
                                                    $json = [
                                                        'error' => 0,
                                                        'message' => 'El participante se registro correctamente pero el perfil no se pudo subir',
                                                        'data' => $saved_member
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                }
                                            } else {
                                                $json = [
                                                    'error' => 1,
                                                    'message' => $member->errors()
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            }
                                        }
                                    } else {
                                        $json = [
                                            'error' => 1,
                                            'message' => ['birthday' => ['_ageNotAllowed' => 'Debes tener mas de 14 años cumplidos o cumplir 14 años antes del 24 de Octubre']]
                                        ];
                                        $this->response->body(json_encode($json));
                                        return $this->response;
                                    }
                                } elseif ($year_now - $year == 19) {
                                    if ($month >= 10) {
                                        if ($month == 10) {
                                            if ($day >= 24) {
                                                $member = $this->Members->patchEntity($member, $this->request->getData());
                                                $saved_member = $this->Members->save($member);
                                                if ($saved_member) {
                                                    $member_team['team_id'] = $this->request->getData('team_id');
                                                    $member_team['member_id'] = $saved_member->id;
                                                    $this->MembersTeams->save($member_team);
                                                    $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                    if (!is_dir($dir_document)) {
                                                        mkdir($dir_document, 0755, true);
                                                    }
                                                    if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext)) {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El participante se registro correctamente',
                                                            'data' => $saved_member
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    } else {
                                                        $json = [
                                                            'error' => 0,
                                                            'message' => 'El participante se registro correctamente pero el perfil no se pudo subir',
                                                            'data' => $saved_member
                                                        ];
                                                        $this->response->body(json_encode($json));
                                                        return $this->response;
                                                    }
                                                } else {
                                                    $json = [
                                                        'error' => 1,
                                                        'message' => $member->errors()
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                }
                                            } else {
                                                $json = [
                                                    'error' => 1,
                                                    'message' => ['birthday' => ['_ageNotAllowed' => 'Cumples 19 años antes del 24 de Octubre']]
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            }
                                        } else {
                                            $member = $this->Members->patchEntity($member, $this->request->getData());
                                            $saved_member = $this->Members->save($member);
                                            if ($saved_member) {
                                                $member_team['team_id'] = $this->request->getData('team_id');
                                                $member_team['member_id'] = $saved_member->id;
                                                $this->MembersTeams->save($member_team);
                                                $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                                if (!is_dir($dir_document)) {
                                                    mkdir($dir_document, 0755, true);
                                                }
                                                if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext)) {
                                                    $json = [
                                                        'error' => 0,
                                                        'message' => 'El participante se registro correctamente',
                                                        'data' => $saved_member
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                } else {
                                                    $json = [
                                                        'error' => 0,
                                                        'message' => 'El participante se registro correctamente pero el perfil no se pudo subir',
                                                        'data' => $saved_member
                                                    ];
                                                    $this->response->body(json_encode($json));
                                                    return $this->response;
                                                }
                                            } else {
                                                $json = [
                                                    'error' => 1,
                                                    'message' => $member->errors()
                                                ];
                                                $this->response->body(json_encode($json));
                                                return $this->response;
                                            }
                                        }
                                    } else {
                                        $json = [
                                            'error' => 1,
                                            'message' => ['birthday' => ['_ageNotAllowed' => 'Cumples 19 años antes del 24 de Octubre']]
                                        ];
                                        $this->response->body(json_encode($json));
                                        return $this->response;
                                    }
                                } else {
                                    $member = $this->Members->patchEntity($member, $this->request->getData());
                                    $saved_member = $this->Members->save($member);
                                    if ($saved_member) {
                                        $member_team['team_id'] = $this->request->getData('team_id');
                                        $member_team['member_id'] = $saved_member->id;
                                        $this->MembersTeams->save($member_team);
                                        $dir_document = WWW_ROOT . 'docs' . DS . $this->request->getData('team_id');
                                        if (!is_dir($dir_document)) {
                                            mkdir($dir_document, 0755, true);
                                        }
                                        if (move_uploaded_file($this->request->data['profile']['tmp_name'], $dir_document . DS . $saved_member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext)) {
                                            $json = [
                                                'error' => 0,
                                                'message' => 'El participante se registro correctamente',
                                                'data' => $saved_member
                                            ];
                                            $this->response->body(json_encode($json));
                                            return $this->response;
                                        } else {
                                            $json = [
                                                'error' => 0,
                                                'message' => 'El participante se registro correctamente pero el perfil no se pudo subir',
                                                'data' => $saved_member
                                            ];
                                            $this->response->body(json_encode($json));
                                            return $this->response;
                                        }
                                    } else {
                                        $json = [
                                            'error' => 1,
                                            'message' => $member->errors()
                                        ];
                                        $this->response->body(json_encode($json));
                                        return $this->response;
                                    }
                                }
                            } else {
                                $json = [
                                    'error' => 1,
                                    'message' => ['birthday' => ['_ageNotAllowed' => 'Debes tener entre 14 y 18 años']]
                                ];
                                $this->response->body(json_encode($json));
                                return $this->response;
                            }
                        } else {
                            $json = [
                                'error' => 1,
                                'message' => 'Tipo incorrecto'
                            ];
                            $this->response->body(json_encode($json));
                            return $this->response;
                        }
                    } else {
                        $json = [
                            'error' => 1,
                            'message' => ['profile' => ['_invalidFormat' => 'Formato de archivo no válido']]
                        ];
                        $this->response->body(json_encode($json));
                        return $this->response;
                    }
                } else {
                    $json = [
                        'error' => 1,
                        'message' => ['profileSize' => ['_sizeNotAllowed' => 'Tamaño de archivo supera el limite']]
                    ];
                    $this->response->body(json_encode($json));
                    return $this->response;
                }
            }
        }
    }

    public function sendEmail(){
        $this->autoRender = false;
            $email = new Email('default');
            $email->from(['robotica.first.bolivia@gmail.com' => 'Robótica'])
                ->to($this->request->getData('email'))
                ->subject('CONFIRMACIÓN TORNEO DE ROBÓTICA 2019')
                ->send('Estimado Tutor/Participante:

¡Bienvenido a Bolivia Robotics - 2º Torneo Nacional de Robótica!

Por favor, lea con detenimiento la convocatoria que se encuentra en http://robotica.usfx.bo/

Una vez termine el plazo de inscripción nos comunicaremos con ustedes por correo electrónico con las especificaciones de la siguiente etapa.

Saludos.

EQUIPO ORGANIZADOR BOLIVIA ROBOTICS 2019');
    }

    public function sendEmailLineFollower(){
        $this->autoRender = false;
            $email = new Email('default');
            $email->from(['robotica.first.bolivia@gmail.com' => 'Robótica'])
                ->to($this->request->getData('email'))
                ->subject('CONFIRMACIÓN DE INSCRIPCIÓN TORNEO DE ROBÓTICA 2019')
                ->send('Estimado Participante:

¡Bienvenido a Bolivia Robotics - 2º Torneo Nacional de Robótica!

Por favor, lea con detenimiento la convocatoria que se encuentra en http://robotica.usfx.bo/

Una vez termine el plazo de inscripción nos comunicaremos con ustedes por correo electrónico con las especificaciones para la siguiente etapa.

Saludos.

EQUIPO ORGANIZADOR BOLIVIA ROBOTICS 2019');
    }

    public function contactUs() {
        $this->autoRender = false;
        $email = new Email('default');
        $email->from(['robotica.first.bolivia@gmail.com' => $this->request->getData('name')])
            ->to('robotica.first.bolivia@gmail.com')
            ->subject($this->request->getData('subject'))
            ->send('De: ' . $this->request->getData('email_sender') . '
Celular: ' . $this->request->getData('cellphone') . '
' . $this->request->getData('message'));
    }

    public function registerEmail() {
        $this->autoRender = false;
        $this->loadModel('Teams');
        $team = $this->Teams
        ->find()
        ->contain(['Members'])
        ->where(['id' => $this->request->getData('team_id')])
        ->first();

        // echo json_encode($team);
        $competitors = '';
        foreach ($team['members'] as $member) {
            if($member['type'] == 'T'){
                $tutor = $member;
            } else{
                $competitors .= $member['surname'] . ' ' . $member['name'] . '
                        ';
            }
        }

        $email = new Email('default');
        $email->from(['robotica.first.bolivia@gmail.com' => 'Robótica'])
            ->to('said.perez.poppe@gmail.com')
            ->subject('Nueva inscripción')
            ->send('Se registró un nuevo equipo para el Torneo Nacional de Robótica en la Categoría First Global:
Nombre de equipo: ' . $team['name'] . '
Respuesta: ' . $team['answer'] . '
Departamento: ' . $team['department'] . '
Ciudad: ' . $team['city'] . '
Tutor: ' .  $tutor['surname'] . ' ' .$tutor['name'] . '
Participantes: ' . $competitors
        );
    }

    /**
     * Edit method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $member = $this->Members->patchEntity($member, $this->request->getData());
            if ($this->Members->save($member)) {
                $this->Flash->success(__('The member has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The member could not be saved. Please, try again.'));
        }
        $teams = $this->Members->Teams->find('list', ['limit' => 200]);
        $this->set(compact('member', 'teams'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $member = $this->Members->get($id);
        if ($this->Members->delete($member)) {
            $this->Flash->success(__('The member has been deleted.'));
        } else {
            $this->Flash->error(__('The member could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getDocument($memberId = null, $teamId = null)
    {
        $member = $this->Members->get($memberId, [
            'contain' => ['Teams']
        ]);

        $dir_document = WWW_ROOT . 'docs' . DS . $teamId;
        $ext = 'pdf';
        // Store the file name into variable 
        $filename = Inflector::slug(strtolower($member->name), '_') . '.' . $ext; 
        $file = $dir_document . DS . $member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext;

        if (!file_exists(file)) {
            $ext = '.docx';
            $file = $dir_document . DS . $member->id . '_' . Inflector::slug(strtolower($member->surname), '_') . '_' . Inflector::slug(strtolower($member->name), '_') . '.' . $ext;
        }

        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for internet explorer
        header("Content-Type: octet-stream");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:".filesize($attachment_location));
        header("Content-Disposition: attachment; filename=" . $filename);
        readfile($attachment_location);
        die();  
    }
}
