<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Teams Model
 *
 * @property \App\Model\Table\MembersTable|\Cake\ORM\Association\BelongsToMany $Members
 *
 * @method \App\Model\Entity\Team get($primaryKey, $options = [])
 * @method \App\Model\Entity\Team newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Team[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Team|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Team patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Team[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Team findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TeamsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teams');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Members', [
            'foreignKey' => 'team_id',
            'targetForeignKey' => 'member_id',
            'joinTable' => 'members_teams'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
     public function validationDefault(Validator $validator)
     {
         $validator
             ->integer('id')
             ->allowEmpty('id', 'create');

         $validator
             ->scalar('name')
             ->maxLength('name', 30)
             ->requirePresence('name', 'create')
             ->notEmpty('name', 'Este campo es requerido');

         $validator
             ->scalar('answer')
             ->requirePresence('answer', 'create')
             ->notEmpty('answer', 'Este campo es requerido');

         $validator
             ->scalar('city')
             ->maxLength('city', 35)
             ->requirePresence('city', 'create')
             ->notEmpty('city', 'Este campo es requerido');

         $validator
             ->scalar('department')
             ->maxLength('department', 25)
             ->requirePresence('department', 'create')
             ->notEmpty('department', 'Este campo es requerido');

         return $validator;
     }

     public function buildRules(RulesChecker $rules)
     {
         $rules->add($rules->isUnique(['name'], 'El nombre de equipo ya está registrado'));

         return $rules;
     }
}
