<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Members Model
 *
 * @property \App\Model\Table\TeamsTable|\Cake\ORM\Association\BelongsToMany $Teams
 *
 * @method \App\Model\Entity\Member get($primaryKey, $options = [])
 * @method \App\Model\Entity\Member newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Member[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Member|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Member patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Member[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Member findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MembersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('members');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Teams', [
            'foreignKey' => 'member_id',
            'targetForeignKey' => 'team_id',
            'joinTable' => 'members_teams'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
        ->integer('id')
        ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 40)
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'Este campo es requerido');

        $validator
            ->scalar('surname')
            ->maxLength('surname', 40)
            ->requirePresence('surname', 'create')
            ->notEmpty('surname', 'Este campo es requerido');

        $validator
            ->scalar('identity_card')
            ->maxLength('identity_card', 14)
            ->requirePresence('identity_card', 'create')
            ->notEmpty('identity_card', 'Este campo es requerido');

        $validator
            ->date('birthday')
            ->requirePresence('birthday', 'create')
            ->notEmpty('birthday', 'Este campo es requerido');

        $validator
            ->email('email', '', 'Por favor ingrese un email válido')
            ->requirePresence('email', 'create')
            ->notEmpty('email', 'Este campo es requerido');

        $validator
            ->scalar('cellphone')
            ->maxLength('cellphone', 8)
            ->requirePresence('cellphone', 'create')
            ->notEmpty('cellphone', 'Este campo es requerido');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 100)
            ->requirePresence('institution', 'create')
            ->notEmpty('institution', 'Este campo es requerido');

        $validator
            ->scalar('type')
            ->maxLength('type', 1)
            ->requirePresence('type', 'create')
            ->notEmpty('type', 'Este campo es requerido');

        $validator
            ->integer('robots_quantity')
            ->allowEmpty('robots_quantity', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['email'], 'El email ya está registrado'));
        // $rules->add($rules->isUnique(['identity_card'], 'El ci ya está registrado'));

        return $rules;
    }
}
