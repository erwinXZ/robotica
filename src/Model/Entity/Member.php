<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Member Entity
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $identity_card
 * @property \Cake\I18n\FrozenDate $birthday
 * @property string $email
 * @property string $cellphone
 * @property string $institution
 * @property string $type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Team[] $teams
 */
class Member extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'surname' => true,
        'identity_card' => true,
        'birthday' => true,
        'email' => true,
        'cellphone' => true,
        'institution' => true,
        'type' => true,
        'robots_quantity' => true,
        'created' => true,
        'modified' => true,
        'teams' => true
    ];
}
